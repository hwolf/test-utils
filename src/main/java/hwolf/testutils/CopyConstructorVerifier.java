/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.testutils;

import org.springframework.util.ReflectionUtils;

import java.beans.PropertyDescriptor;

import static org.assertj.core.api.Assertions.assertThat;

public final class CopyConstructorVerifier extends PropertyVerifier {

    private CopyConstructorVerifier(Class<?> type) {
        super(type);
    }

    @Override
    public void verify() {
        Object source = newInstance();
        for (PropertyDescriptor property : getProperties()) {
            if (shouldTestProperty(property)) {
                Object value = newPropertyValue(property);
                setPropertyValue(property, source, value);
            }
        }
        Class<?> type = source.getClass();
        Object copy = createCopy(source, type);
        assertThat(copy).isEqualToIgnoringGivenFields(source, excludes());
    }

    private Object createCopy(Object source, Class<?> type) {
        try {
            return type.getConstructor(type).newInstance(source);
        } catch (Exception ex) {
            ReflectionUtils.handleReflectionException(ex);
            return null;
        }
    }

    public static CopyConstructorVerifier forClass(Class<?> type) {
        return new CopyConstructorVerifier(type);
    }
}
