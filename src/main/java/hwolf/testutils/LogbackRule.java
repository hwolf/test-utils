/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.testutils;

import com.google.common.collect.Iterables;
import org.junit.rules.ExternalResource;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;

public class LogbackRule extends ExternalResource {

    private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    private Level oldLevel;
    private ListAppender<ILoggingEvent> appender;

    public ILoggingEvent getLoggingEvent() {
        return Iterables.getLast(appender.list, null);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void before() throws Throwable {
        appender = new ListAppender<>();
        appender.start();
        LOGGER.addAppender(appender);

        oldLevel = LOGGER.getLevel();
        LOGGER.setLevel(Level.ALL);
    }

    @Override
    protected void after() {
        LOGGER.setLevel(oldLevel);

        LOGGER.detachAppender(appender);
        appender.stop();
    }
}
