/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.testutils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.rules.ExternalResource;

/**
 * @see <a href="https://cleantestcode.wordpress.com/2014/04/12/jodatime-and-junit/">JodaTime and JUnit</a>
 */
public class FixedDateTimeRule extends ExternalResource {

    private final DateTime dt;

    public FixedDateTimeRule(DateTime dt) {
        this.dt = dt;
    }

    @Override
    protected void before() throws Throwable {
        DateTimeUtils.setCurrentMillisFixed(dt.getMillis());
    }

    @Override
    protected void after() {
        DateTimeUtils.setCurrentMillisSystem();
    }
}
