package hwolf.testutils;

import java.beans.PropertyDescriptor;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Automates JUnit testing of simple getter / setter methods.
 * <p/>
 * <p>
 * This class was modeled after the {@link nl.jqno.equalsverifier.EqualsVerifier} approach where in
 * a few lines of code, you can test the entirety of a simple Java object. For example:
 * </p>
 * <p/>
 * <pre>
 * GetterSetterVerifier.forClass(MyClass.class).verify();
 * </pre>
 * <p/>
 * <p>
 * You can also specify which properties you do no want to test in the event that the associated
 * getters and setters are non-trivial. For example:
 * </p>
 * <p/>
 * <pre>
 * GetterSetterVerifier.forClass(MyClass.class).exclude("someComplexProperty").exclude("anotherComplexProperty")
 *         .verify();
 * </pre>
 * <p/>
 * <p>
 * On the other hand, if you'd rather be more verbose about what properties are tested, you can
 * specify them using the include syntax. When using the include approach, only the properties that
 * you specified will be tested. For example:
 * </p>
 * <p>
 * <pre>
 * GetterSetterVerifier.forClass(MyClass.class).include("someSimpleProperty").include("anotherSimpleProperty").verify();
 * </pre>
 *
 * @author pitz@indeed.com (Jeremy Pitzeruse)
 * @see <a href="http://www.jqno.nl/equalsverifier/">www.jqno.nl/equalsverifier</a>
 */
public final class GetterSetterVerifier extends PropertyVerifier {

    /**
     * Creates a getter / setter verifier to test properties for a particular class.
     *
     * @param type The class that we are testing
     */
    private GetterSetterVerifier(Class<?> type) {
        super(type);
    }

    /**
     * Verify the class's getters and setters
     */
    @Override
    public void verify() {
        for (PropertyDescriptor property : getProperties()) {
            if (shouldTestProperty(property)) {
                testProperty(property);
            }
        }
    }

    /**
     * Test an individual property by getting the read method and write method and passing the
     * default value for the type to the setter and asserting that the same value was returned.
     *
     * @param property The property that we are testing.
     */
    private void testProperty(PropertyDescriptor property) {
        Object expected = newPropertyValue(property);
        testProperty(property, expected);

        if (!property.getPropertyType().isPrimitive()) {
            testProperty(property, null);
        }
    }

    private void testProperty(PropertyDescriptor property, Object expected) {
        Object target = newInstance();
        setPropertyValue(property, target, expected);
        Object actual = getPropertyValue(property, target);

        assertThat(actual).overridingErrorMessage(
                "Getter/setter of property %s does not produce the same result: Expected <%s>, actual <%s>",
                property.getDisplayName(), expected, actual) //
                .isEqualTo(expected);
    }

    /**
     * Factory method for easily creating a test for the getters and setters.
     *
     * @param type The class that we are testing the getters and setters for.
     * @return An object that can be used for testing the getters and setters of a class.
     */
    public static GetterSetterVerifier forClass(Class<?> type) {
        return new GetterSetterVerifier(type);
    }
}
