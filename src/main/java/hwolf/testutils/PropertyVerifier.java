/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.testutils;

import com.google.common.collect.Sets;
import nl.jqno.equalsverifier.JavaApiPrefabValues;
import nl.jqno.equalsverifier.internal.objenesis.ObjenesisStd;
import nl.jqno.equalsverifier.util.Instantiator;
import nl.jqno.equalsverifier.util.PrefabValues;
import org.springframework.beans.BeanUtils;
import org.springframework.util.ReflectionUtils;

import java.beans.PropertyDescriptor;
import java.util.Set;

public abstract class PropertyVerifier {

    private final Class<?> type;
    private final PrefabValues values;

    private Set<String> excludes;

    public PropertyVerifier(Class<?> type) {
        this.type = type;
        this.values = new PrefabValues();
        JavaApiPrefabValues.addTo(values);
    }

    public <S> PropertyVerifier withPrefabValue(Class<S> otherType, S value) {
        values.put(otherType, value, value);
        return this;
    }

    public PropertyVerifier exclude(String exclude) {
        if (excludes == null) {
            excludes = Sets.newHashSet();
        }
        excludes.add(exclude);
        return this;
    }

    public abstract void verify();

    protected Object newInstance() {
        return new ObjenesisStd().newInstance(type);
    }

    protected Object newPropertyValue(PropertyDescriptor property) {
        Class<?> propertyType = property.getPropertyType();
        if (values.contains(propertyType)) {
            return values.getRed(propertyType);
        }
        try {
            return Instantiator.of(propertyType).instantiate();
        } catch (Exception ignored) {
            // If it fails for some reason, just return null.
            return null;
        }
    }

    protected PropertyDescriptor[] getProperties() {
        return BeanUtils.getPropertyDescriptors(type);
    }

    protected Object getPropertyValue(PropertyDescriptor property, Object instance) {
        return ReflectionUtils.invokeMethod(property.getReadMethod(), instance);
    }

    protected void setPropertyValue(PropertyDescriptor property, Object instance, Object value) {
        ReflectionUtils.invokeMethod(property.getWriteMethod(), instance, value);
    }

    protected boolean shouldTestProperty(PropertyDescriptor property) {
        if (property.getWriteMethod() == null || property.getReadMethod() == null) {
            return false;
        }
        return excludes == null || !excludes.contains(property.getDisplayName());
    }

    protected String[] excludes() {
        if (excludes == null) {
            return new String[0];
        }
        return excludes.toArray(new String[excludes.size()]);
    }
}
